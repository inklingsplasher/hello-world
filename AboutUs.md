__**About Us**__  

**Welcome to InkCurity!**  
We are a *small server* all about **security in the internet**.  
We'll assist you when it comes to:  

`*` Security-Breaches  
`*` Rooting / Jailbreaking Android / iOS  
`*` Managing Linux-Servers  
`*` Supporting **your** project!  

**Our goal** is to ~~keep~~ make the internet a safer place to surf in and **support various projects** who try 
to do the same.  
